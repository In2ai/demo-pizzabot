# demo-PizzaBot

Proyecto creado en el [RASA Community Chapter Madrid el día 14 de mayo de 2020](https://www.youtube.com/watch?v=psi9l2IekCA). Chatbot basado en pedir pizzas.
Solo es una demo, no es un producto final. Para quien se atreva, puede descargar el proyecto y mejorarlo tanto como pueda.

Para probarlo habrá que seguir los siguientes pasos (recomendado uso de virtual environment y Python3):
1. Crear entorno virtual (opcional): 
    ```
    pip install virtualenv
    virtualenv nombre_de_tu_entorno -p python3
    ```
2. Instalar rasa: 
    ```
    pip install rasa
    ```
3. Instalar Spacy y cargar el modulo en español:
    ```
    pip install spacy
    python -m spacy download es_core_news_sm
    ```
4. Con una terminal independiente activar las custom actions: 
    ```
    rasa run actions
    ```
5. Y por ultimo, con otra terminal activaremos la consola para hablar con el bot:
    ```
    rasa shell
    ```
   
Para más conocimientos sobre Rasa visiten la [documentacion oficial](https://rasa.com/docs/rasa/user-guide/installation/) y/o unanse al espacio habilitado en [slack para la comunidad de Rasa en España](https://join.slack.com/t/rasamadridchapter/shared_invite/zt-e5qwgpc7-yXvy8F_pY398_ABcyAUqjw)