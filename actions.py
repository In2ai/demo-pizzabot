
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet, EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction


class ActionCuenta(Action):

    precios = {'carbonara': 2, 'hawaiana': 3, 'margarita': 4, 'cuatro quesos': 5, 'barbacoa': 6}

    def name(self) -> Text:
        return "action_cuenta"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        pizza = tracker.get_slot('pizza')
        cuenta = tracker.get_slot('cuenta')
        cuenta += self.precios[pizza]

        return [SlotSet('cuenta', cuenta)]


class FormPizza(FormAction):

    def name(self) -> Text:
        return 'form_pizza'

    @staticmethod
    def required_slots(tracker: "Tracker") -> List[Text]:
        return ['pizza']

    def submit(
        self,
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: Dict[Text, Any],
    ) -> List[EventType]:
        return []