## intent:pedir
- quiero pedir pizzas
- quiero pizzas
- querria pedir
- querria pedir pizzas

## intent:carta
- que pizzas hay?
- ¿Que tipo de pizzas hay?
- quiero saber que pizzas hay?

## intent:sabor_pizza
- quiero una pizza [carbonara](pizza)
- una [hawaiana](pizza)
- querria una [cuatro quesos](pizza)
- una [margarita](pizza)
- una [4 quesos](pizza)
- quiero una [jabuallana](pizza)
- una [barbacoa](pizza)
- querria una [bbc](pizza)
- querria una [varvacoa](pizza)
- quiero una de [jamon york](pizza)
- una jabuallana
- querria una [hawaiana](pizza)
- una [jabuallana](pizza)
- [cuatro quesos](pizza)
- [jabuallana](pizza)

## intent:nombre
- Me llamo Alfonso
- Me llamo Rafa
- Soy Lino
- Mi nombre es Miguel Angel

## lookup:pizza
data/lookup_tables/pizzas.txt

## synonym:hawaiana
- jabuallana

## synonym:barbacoa
- bbc
- varvacoa

## synonym:cuatro quesos
- 4 quesos

## synonym:margarita
- jamon york

## synonym:carbonara
- carbonata